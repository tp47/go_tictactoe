FROM golang AS base
WORKDIR /tictactoe/
COPY . .

FROM base as tester
ENTRYPOINT ["go","test","./..."]

FROM base as builder
RUN mkdir empty
RUN CGO_ENABLED=0 go build -o api_server api/tictactoe/main.go

FROM scratch as server
COPY --from=builder /tictactoe/api_server .
COPY --from=builder /tictactoe/empty /tmp
EXPOSE 8080
ENTRYPOINT ["./api_server"]
