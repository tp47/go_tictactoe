package core

import "testing"

func TestMakeMove(t *testing.T) {
	t.Run("GUEST makes a move on a blank board", func(t *testing.T) {
		new := New()

		next := makeMove(new, 0, GUEST)

		expected := new
		expected.Moves += 1
		expected.Board[0] = GUEST

		if expected != next {
			t.Errorf("Unexpected game state")
		}
	})
	t.Run("Game is over with the last move", func(t *testing.T) {
		new := New()
		new.Board[0] = GUEST
		new.Board[1] = GUEST
		new.Moves = 2

		expected := new
		expected.Board[2] = GUEST
		expected.Moves = 3
		expected.IsGameOver = true
		expected.Winner = GUEST

		next := makeMove(new, 2, GUEST)

		if expected != next {
			t.Errorf("Unexpected game state \n Expected:%v \n Got: %v", expected, next)
		}
	})
	t.Run("An invalid move doesn't change game state", func(t *testing.T) {
		new := New()
		next := makeMove(new, 10, GUEST)
		if new != next {
			t.Errorf("Expected no change")
		}
	})
}
