package core

func getAvailable(board GameBoard) []int {
	// provides indices of available spots
	var avail []int
	for i, square := range board {
		if square == EMPTY {
			avail = append(avail, i)
		}
	}
	return avail
}

func cpuMakeMove(current GameState) GameState {
	// make a move in the game and returns new GameState
	location := getAvailable(current.Board)[0]
	return makeMove(current, location, HOST)
}
