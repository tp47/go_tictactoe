package core

import "testing"

func TestCalcWinner(t *testing.T) {
	t.Run("No one wins on a blank board", func(t *testing.T) {
		gameBoard := GameBoard{
			EMPTY, EMPTY, EMPTY,
			EMPTY, EMPTY, EMPTY,
			EMPTY, EMPTY, EMPTY,
		}

		winner := CalcWinner(gameBoard)

		if winner != EMPTY {
			t.Errorf("Expected no one to win")
		}
	})

	t.Run("When host has winning combination", func(t *testing.T) {
		gameBoard := GameBoard{
			EMPTY, EMPTY, EMPTY,
			HOST, HOST, HOST,
			EMPTY, EMPTY, EMPTY,
		}

		winner := CalcWinner(gameBoard)

		if winner != HOST {
			t.Errorf("Expected HOST to win")
		}
	})

	t.Run("Test all unique winning combinations for GUEST", func(t *testing.T) {
		testCases := []struct {
			desc   string
			board  GameBoard
			winner GameCell
		}{
			{
				"GUEST left to right diagonal win",
				GameBoard{
					GUEST, EMPTY, EMPTY,
					EMPTY, GUEST, EMPTY,
					EMPTY, EMPTY, GUEST,
				},
				GUEST,
			},
			{
				"GUEST right to left diagonal win",
				GameBoard{
					EMPTY, EMPTY, GUEST,
					EMPTY, GUEST, EMPTY,
					GUEST, EMPTY, EMPTY,
				},
				GUEST,
			},
			{
				"GUEST vertical win",
				GameBoard{
					EMPTY, GUEST, EMPTY,
					EMPTY, GUEST, EMPTY,
					EMPTY, GUEST, EMPTY,
				},
				GUEST,
			},
			{
				"GUEST horizontal win",
				GameBoard{
					EMPTY, EMPTY, EMPTY,
					GUEST, GUEST, GUEST,
					EMPTY, EMPTY, EMPTY,
				},
				GUEST,
			},
			{
				"HOST diagonal win",
				GameBoard{
					HOST, EMPTY, EMPTY,
					EMPTY, HOST, EMPTY,
					EMPTY, EMPTY, HOST,
				},
				HOST,
			},
			{
				"HOST vertical win",
				GameBoard{
					EMPTY, HOST, EMPTY,
					EMPTY, HOST, EMPTY,
					EMPTY, HOST, EMPTY,
				},
				HOST,
			},
			{
				"HOST horizontal win",
				GameBoard{
					EMPTY, EMPTY, EMPTY,
					HOST, HOST, HOST,
					EMPTY, EMPTY, EMPTY,
				},
				HOST,
			},
		}

		for _, tt := range testCases {
			t.Run(tt.desc, func(t *testing.T) {
				winner := CalcWinner(tt.board)
				if winner != tt.winner {
					t.Error("The winner should be GUEST, not ", winner)
				}
			})
		}
	})

}
