package core

func getRow(board GameBoard, r int) []GameCell {
	row := make([]GameCell, 0)
	for i := 0; i < WIDTH; i++ {
		row = append(row, board[(r*WIDTH)+i])
	}
	return row

}

func getColumn(board GameBoard, c int) []GameCell {
	col := make([]GameCell, 0)
	for i := 0; i < HEIGHT; i++ {
		col = append(col, board[c+(i*HEIGHT)])
	}
	return col

}

func getDiagonal(board GameBoard, isFirst bool) []GameCell {
	diag := make([]GameCell, 0)
	prod := HEIGHT * WIDTH
	avg := int((HEIGHT + WIDTH) / 2)
	if isFirst {
		for i := 0; i < prod; i = i + avg + 1 {
			diag = append(diag, board[i])
		}
	} else {
		for i := avg - 1; i < prod-WIDTH+1; i = i + avg - 1 {
			diag = append(diag, board[i])
		}
	}
	return diag
}

func allSameCells(a []GameCell) bool {
	if a[0] == EMPTY {
		return false
	}
	for i := 1; i < len(a); i++ {
		if a[i] != a[0] {
			return false
		}
	}
	return true
}

func CalcWinner(current GameBoard) GameCell {
	// returns string of winner or " " when no one wins
	board := current
	var myRow []GameCell
	var myCol []GameCell

	// Check for Horizontal wins
	for i := 0; i < WIDTH; i++ {
		myRow = getRow(board, i)
		if allSameCells(myRow) {
			return myRow[0]
		}
	}

	// Check for Vertical wins
	for i := 0; i < HEIGHT; i++ {
		myCol = getColumn(board, i)
		if allSameCells(myCol) {
			return myCol[0]
		}
	}

	// Check for Diagonal wins
	left_to_right_diag := getDiagonal(board, true)
	if allSameCells(left_to_right_diag) {
		return left_to_right_diag[0]
	}
	right_to_left_diag := getDiagonal(board, false)
	if allSameCells(right_to_left_diag) {
		return right_to_left_diag[0]
	}

	return EMPTY // when no one wins
}
