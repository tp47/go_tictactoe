package core

func New() GameState {
	// create a new blank game
	newGame := GameState{}
	for i := 0; i < 9; i++ {
		newGame.Board[i] = EMPTY
	}
	return newGame
}

func validateMove(current GameBoard, location int) bool {
	if location > 8 || location < 0 || current[location] == GUEST ||
		current[location] == HOST {
		return false // Make this an error?
	}

	return true
}

func isGameOver(current GameState) (bool, GameCell) {
	winner := CalcWinner(current.Board)
	if winner != EMPTY || current.Moves == MAXMOVES {
		return true, winner
	}
	return false, EMPTY
}

// make a move in the game and returns new GameState
func makeMove(current GameState, location int, player GameCell) GameState {

	if !validateMove(current.Board, location) {
		return current
	}

	new := current
	new.Board[location] = player
	new.Moves++

	new.IsGameOver, new.Winner = isGameOver(new)

	return new
}

func PlayerMakeMove(current GameState, loc int) GameState {
	// Allows player to make a move and responds with the game state
	// after the cpu also makes a move (if the game is not already over)

	afterUserMove := makeMove(current, loc, GUEST)

	if afterUserMove.IsGameOver || afterUserMove == current {
		return afterUserMove
	}

	return cpuMakeMove(afterUserMove)
}
