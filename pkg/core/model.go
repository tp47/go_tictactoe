package core

const HEIGHT = 3
const WIDTH = 3
const MAXMOVES int = HEIGHT * WIDTH

type GameCell string
type GameBoard = [HEIGHT * WIDTH]GameCell
type GameState struct {
	// keeps track of game
	Board      GameBoard `json:"board"`
	Moves      int       `json:"moves"`
	IsGameOver bool      `json:"gameover"`
	ID         string    `json:"id"`
	Winner     GameCell  `json:"winner"`
}

const GUEST GameCell = "❌"
const HOST GameCell = "⭕"
const EMPTY GameCell = ""
