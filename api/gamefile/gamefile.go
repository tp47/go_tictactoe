package gamefile

import (
	"encoding/json"
	"io/ioutil"
	"tictactoe/pkg/core"
)

func WriteGameState(game core.GameState, gameID string) (filedata []byte, err error) {
	game.ID = gameID
	filedata, _ = json.MarshalIndent(game, "", " ")
	err = ioutil.WriteFile(filepath(gameID), filedata, 0644)
	return
}

func LoadGameState(gameID string) (game core.GameState, err error) {
	game.ID = gameID
	data, read_err := ioutil.ReadFile(filepath(gameID))
	err = read_err
	if err != nil {
		return
	}
	err = json.Unmarshal(data, &game)
	return
}

func filepath(gameID string) string {
	return "/tmp/" + gameID + ".json"
}
