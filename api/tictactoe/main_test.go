package main

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"tictactoe/api/handler"
)

const gameIdLen = 6
const serverURL = "http://localhost:8080"

func TestNew(t *testing.T) {
	t.Run("Create a new game", func(t *testing.T) {
		req, err := http.NewRequest("GET", serverURL+"/new/", nil)
		if err != nil {
			t.Fatalf("Cannot create game: %v", err)
		}
		rec := httptest.NewRecorder()
		handler.New(rec, req)

		res := rec.Result()
		defer res.Body.Close()

		b, err := ioutil.ReadAll(res.Body)
		if err != nil {
			t.Fatalf("Could not read response %v", err)
		}

		d := string(bytes.TrimSpace(b))
		if len(d) != gameIdLen {
			t.Fatalf("Incorrect game id length")
		}
	})

}

func TestMove(t *testing.T) {
	t.Run("Create a new game", func(t *testing.T) {
		req, err := http.NewRequest("POST", serverURL+"/move/", nil)
		if err != nil {
			t.Fatalf("Cannot creat game: %v", err)
		}
		rec := httptest.NewRecorder()
		handler.New(rec, req)

		res := rec.Result()
		defer res.Body.Close()

		b, err := ioutil.ReadAll(res.Body)
		if err != nil {
			t.Fatalf("Could not read response %v", err)
		}

		d := string(bytes.TrimSpace(b))
		if len(d) != gameIdLen {
			t.Fatalf("Incorrect game id length")
		}
	})

}
