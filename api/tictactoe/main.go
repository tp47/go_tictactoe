package main

import (
	"log"
	"net/http"
	"tictactoe/api/handler"
)

func main() {
	http.HandleFunc("/new/", handler.New)
	http.HandleFunc("/move/", handler.Move)

	log.Fatal(http.ListenAndServe(":8080", nil))

}
