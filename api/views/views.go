package views

import (
	"fmt"
	"regexp"
	"strconv"
	"tictactoe/api/gamefile"
	"tictactoe/pkg/core"
)

const (
	seed  = 10
	idLen = 6
	idRE  = "^[a-zA-Z0-9]+$"
)

// Use core pkg to creat a new game state and generate a hash
func NewGameState() (gameID string, errors []error) {
	gameID = NewSHA1Hash(seed)[:idLen]
	_, e := gamefile.WriteGameState(core.New(), gameID)
	errors = []error{e}
	return
}

// Use core pkg to calculate next state of the game
func NextGameState(id, move string) (next core.GameState, errors []error) {
	var (
		game, loadErr        = getGame(id)
		current, gameoverErr = checkGameOver(game)
		loc, parseErr        = parseMove(move)
		errlist              = [3]error{loadErr, parseErr, gameoverErr}
	)

	for _, e := range errlist {
		if e != nil {
			errors = append(errors, e)
		}
	}

	if len(errors) != 0 {
		next = current
		return
	}

	next = core.PlayerMakeMove(current, loc)
	return
}

func getGame(id string) (core.GameState, error) {
	blank := core.GameState{}
	validID := regexp.MustCompile(idRE)
	//validate id
	if len(id) != idLen || validID.FindStringSubmatch(id) == nil {
		return blank, fmt.Errorf("Invalid game ID: %q", id)
	}

	if current, readErr := gamefile.LoadGameState(id); readErr != nil {
		return blank, fmt.Errorf("Game doesn't exsist!")
	} else {
		return current, nil
	}
}

func parseMove(move string) (int, error) {
	if loc, locErr := strconv.Atoi(move); locErr != nil || loc < 0 || loc > core.MAXMOVES {
		return -1, fmt.Errorf("Could not understand user move: %s", move)
	} else {
		return loc - 1, nil // program logic counts from 0
	}
}

func checkGameOver(current core.GameState) (core.GameState, error) {
	if current.IsGameOver {
		return current, fmt.Errorf("Game over, winner is %s", current.Winner)
	}
	return current, nil
}
