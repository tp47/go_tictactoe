package views

import (
	"testing"
)

var gameID string

func filterNil(t *testing.T, es []error) (errors []error) {
	t.Helper()
	for _, e := range es {
		if e != nil {
			errors = append(errors, e)
		}
	}
	return
}

func TestNewGameState(t *testing.T) {
	t.Run("Create new game", func(t *testing.T) {
		new, errs := NewGameState()
		gameID = new
		errors := filterNil(t, errs)
		if len(errors) != 0 {
			t.Fatalf("Could not initialize game:\n %q", errors)
		}
		if len(new) != idLen {
			t.Fatalf("Game id is incorrect: %v", new)
		}
	})
}
func TestNextGameState(t *testing.T) {
	t.Run("Move to position 1 on an empty board", func(t *testing.T) {
		game, errs := NextGameState(gameID, "1")

		errors := filterNil(t, errs)
		if len(errors) != 0 {
			t.Fatalf("Could not make next move:\n %v", errors)
		}

		if game.Moves != 2 {
			t.Fatalf("Move not registered: %v", game)
		}
	})
	t.Run("Invalid move on an empty board", func(t *testing.T) {
		_, errs := NextGameState(gameID, "abc")

		errors := filterNil(t, errs)
		if len(errors) != 1 {
			t.Errorf("Expected an error about invalid move")
		}
	})
	t.Run("Invalid game ID", func(t *testing.T) {
		_, errs := NextGameState("", "1")

		errors := filterNil(t, errs)
		if len(errors) != 1 {
			t.Errorf("Expected an invalid game ID move")
		}
	})
	t.Run("Invalid game ID and move", func(t *testing.T) {
		_, errs := NextGameState("", "")

		errors := filterNil(t, errs)
		if len(errors) != 2 {
			t.Errorf("Expected two errors")
		}
	})
}
