package handler

import (
	"fmt"
	"log"
	"net/http"
	"regexp"
	"tictactoe/api/gamefile"
	"tictactoe/api/views"
)

var (
	New       = makeHandler(newGameHandler)
	Move      = makeHandler(makeMoveHandler)
	validPath = regexp.MustCompile("^/(new|move)/")
)

func newGameHandler(w http.ResponseWriter, _ *http.Request) {
	data, e := views.NewGameState()
	if e[0] != nil {
		log.Fatalf("Failed to create a new game: %q", e)
	}
	w.Write([]byte(data))
}

func makeMoveHandler(w http.ResponseWriter, r *http.Request) {
	next, errs := views.NextGameState(r.FormValue("id"), r.FormValue("move"))
	if len(errs) != 0 {
		http.Error(w, errs2json(errs), 500)
		return
	}

	data, writeErr := gamefile.WriteGameState(next, next.ID)

	if writeErr != nil {
		http.Error(w, errs2json([]error{writeErr}), 500)
		return
	}

	w.Write(data)
}

func makeHandler(fn func(http.ResponseWriter, *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r)
	}
}

func errs2json(es []error) (json_str string) {
	json_str = "{\"errors\": ["
	for _, e := range es {
		json_str += fmt.Sprintf("\"%v\",", e)
	}
	json_str += "]}"
	return
}
