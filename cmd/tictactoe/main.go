package main

import (
	"tictactoe/cmd/gamefile"
	"tictactoe/cmd/views"
	"tictactoe/pkg/core"
)

func main() {
	// Runs the game on the command line
	game := core.New()
	write_error := gamefile.WriteGameState(game)
	for !(game.IsGameOver) {
		move := views.UserMove(game.Board)
		game = core.PlayerMakeMove(game, move)
		write_error = gamefile.WriteGameState(game)

	}
	_ = write_error //write error no handled
	views.GameOver(game.Board)
}
