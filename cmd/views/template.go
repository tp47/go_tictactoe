package views

const ttt_tmpl = `
      {{.A}}│{{.B}}│{{.C}}
      ──┼──┼──
      {{.D}}│{{.E}}│{{.F}}
      ──┼──┼──
      {{.G}}│{{.H}}│{{.I}}
`
const make_move_tmpl = `
   Make a move: `
const draw_tmpl = `
         Draw

`
const game_won_tmpl = `
        %s wins!

`
