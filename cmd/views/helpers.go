package views

import (
	"bytes"
	"html/template"
	"tictactoe/pkg/core"
)

func stringify(board core.GameBoard) []string {
	// Helper function for structifyboard.
	// Takes a board of gamecells and returns
	// a board of strings.
	var board_strings []string
	for _, cell := range board {
		out := string(cell)
		if cell == " " {
			out = "  "
		}
		board_strings = append(board_strings, out)
	}
	return board_strings
}

func structifyBoard(board core.GameBoard) interface{} {
	// Converts the board array to a string
	stringifiedBoard := stringify(board)
	s := stringifiedBoard
	return struct {
		A, B, C,
		D, E, F,
		G, H, I string
	}{
		s[0], s[1], s[2],
		s[3], s[4], s[5],
		s[6], s[7], s[8],
	}
}

func template2String(myTmpl string, class interface{}) string {
	// Give formatted string.
	// it takes a string template and interface
	t := template.Must(template.New("game").Parse(myTmpl))
	buf := &bytes.Buffer{}
	t.Execute(buf, class)
	clearScreen := "\033[H\033[2J"
	return clearScreen + buf.String()
}
