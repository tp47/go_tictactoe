package views

import (
	"fmt"
	"tictactoe/pkg/core"
)

func UserMove(b core.GameBoard) int {
	// Takes user input to return a location int
	PrintGame(b)
	var move int
	fmt.Printf(make_move_tmpl)
	fmt.Scanf("%d", &move)
	return move - 1 // Humans count from 1
}

func PrintGame(squares core.GameBoard) {
	// Prints the game using a template
	data := structifyBoard(squares)
	output := template2String(ttt_tmpl, data)
	fmt.Printf(output)
}

func GameOver(board core.GameBoard) {
	// Print the final game board and then
	// print winner or msg that it was a draw
	winner := core.CalcWinner(board)
	PrintGame(board)
	if winner == core.EMPTY {
		fmt.Println(draw_tmpl)
	} else {
		output := fmt.Sprintf(game_won_tmpl, string(winner))
		fmt.Printf(output)
	}
}
