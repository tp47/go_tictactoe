package gamefile

import (
	"encoding/json"
	"io/ioutil"
	"tictactoe/pkg/core"
)

func WriteGameState(game core.GameState) error {
	file, _ := json.MarshalIndent(game, "", " ")
	return ioutil.WriteFile("game_state.json", file, 0644)
}
